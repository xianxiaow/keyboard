const keyboard = require('../index');

$(() => {
    const $id1 = $('#id1');
    const $id2 = $('#id2');

    $id1.on('kb:input', function(ev, c, newVal, oldVal){
        if (newVal === '123456') {
            return false;
        }
    })

    $id2.on('kb:ok', function(ev) {
        return false;
        console.log($(this).data('value'));
    })
    setTimeout(() => {
        keyboard.close();
    }, 5000);
});
