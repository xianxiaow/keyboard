/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var keyboard = __webpack_require__(1);
	
	$(function () {
	    var $id1 = $('#id1');
	    var $id2 = $('#id2');
	
	    $id1.on('kb:input', function (ev, c, newVal, oldVal) {
	        if (newVal === '123456') {
	            return false;
	        }
	    });
	
	    $id2.on('kb:ok', function (ev) {
	        return false;
	        console.log($(this).data('value'));
	    });
	    setTimeout(function () {
	        keyboard.close();
	    }, 5000);
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	__webpack_require__(2);
	var xtpl = __webpack_require__(6);
	
	/**
	 * JS 虚拟键盘
	 */
	
	var META_DATA = {
	    height: {
	        plate: {
	            ios: 5.2,
	            android: 4.38
	        },
	        n9: {
	            ios: 5.159,
	            android: 5.44
	        }
	    },
	    id: { // 身份证输入
	        need_tools: true,
	        rows: [{
	            style: 'ukey-9',
	            cel: [{
	                value: 1
	            }, {
	                value: 2
	            }, {
	                value: 3
	            }, {
	                value: 4
	            }, {
	                value: 5
	            }, {
	                value: 6
	            }, {
	                value: 7
	            }, {
	                value: 8
	            }, {
	                value: 9
	            }, {
	                style: 'ukey-gray',
	                value: 'X'
	            }, {
	                value: 0
	            }, {
	                style: 'ukey-gray',
	                value: 'del',
	                name: '<i class="ukey-icon-delete"></i>'
	            }]
	        }]
	    },
	    number: {
	        need_tools: true,
	        rows: [{
	            style: 'ukey-9',
	            cel: [{
	                value: 1
	            }, {
	                value: 2
	            }, {
	                value: 3
	            }, {
	                value: 4
	            }, {
	                value: 5
	            }, {
	                value: 6
	            }, {
	                value: 7
	            }, {
	                value: 8
	            }, {
	                value: 9
	            }, {
	                style: 'ukey-gray',
	                value: '.'
	            }, {
	                value: 0
	            }, {
	                style: 'ukey-gray',
	                value: 'del',
	                name: '<i class="ukey-icon-delete"></i>'
	            }]
	        }]
	    },
	    tel: {
	        need_tools: true,
	        rows: [{
	            style: 'ukey-9',
	            cel: [{
	                value: 1
	            }, {
	                value: 2
	            }, {
	                value: 3
	            }, {
	                value: 4
	            }, {
	                value: 5
	            }, {
	                value: 6
	            }, {
	                value: 7
	            }, {
	                value: 8
	            }, {
	                value: 9
	            }, {
	                style: 'ukey-gray',
	                value: '',
	                name: '&nbsp;'
	            }, {
	                value: 0
	            }, {
	                style: 'ukey-gray',
	                value: 'del',
	                name: '<i class="ukey-icon-delete"></i>'
	            }]
	        }]
	    },
	    plate: {
	        need_tools: false,
	        rows: [{
	            style: 'ukey-p',
	            cel: [{
	                value: '港'
	            }, {
	                value: '澳'
	            }, {
	                style: 'ukey-disable',
	                value: '警'
	            }, {
	                style: 'ukey-disable',
	                value: '学'
	            }, {
	                style: 'ukey-disable',
	                value: '领'
	            }]
	        }, {
	            style: 'ukey-n',
	            cel: [{
	                value: 1
	            }, {
	                value: 2
	            }, {
	                value: 3
	            }, {
	                value: 4
	            }, {
	                value: 5
	            }, {
	                value: 6
	            }, {
	                value: 7
	            }, {
	                value: 8
	            }, {
	                value: 9
	            }, {
	                value: 0
	            }]
	        }, {
	            style: 'ukey-s',
	            cel: [{
	                value: 'Q'
	            }, {
	                value: 'W'
	            }, {
	                value: 'E'
	            }, {
	                value: 'R'
	            }, {
	                value: 'T'
	            }, {
	                value: 'Y'
	            }, {
	                value: 'U'
	            }, {
	                style: 'ukey-disable',
	                value: 'I'
	            }, {
	                style: 'ukey-disable',
	                value: 'O'
	            }, {
	                value: 'P'
	            }]
	        }, {
	            style: 'ukey-s',
	            cel: [{
	                value: 'A'
	            }, {
	                value: 'S'
	            }, {
	                value: 'D'
	            }, {
	                value: 'F'
	            }, {
	                value: 'G'
	            }, {
	                value: 'H'
	            }, {
	                value: 'J'
	            }, {
	                value: 'K'
	            }, {
	                value: 'L'
	            }, {
	                style: 'ukey-delete',
	                value: 'del',
	                name: '<i class="ukey-icon-delete"></i>'
	            }]
	        }, {
	            style: 'ukey-s',
	            cel: [{
	                value: 'Z'
	            }, {
	                value: 'X'
	            }, {
	                value: 'C'
	            }, {
	                value: 'V'
	            }, {
	                value: 'B'
	            }, {
	                value: 'N'
	            }, {
	                value: 'M'
	            }, {
	                style: 'ukey-ok',
	                value: 'ok',
	                name: '完成'
	            }]
	        }]
	    }
	};
	
	/**
	 * 字符串模板
	 */
	var TEMP_STR = '\n{{ if need_tools }}\n<div class="ukey-tools">\n    <div class="ukey-tools-btn">\u5B8C\u6210</div>\n</div>\n{{ endif }}\n<div class="ukey-keys">\n{{ each rows as row }}\n    <div class="ukey-row {{ row.style }}">\n    {{ each row.cel as cel }}<div class="ukey-cel {{ cel.style }}" data-value="{{ cel.value }}">{{ cel.name || cel.value }}</div>{{ endeach }}\n    </div>\n{{ endeach }}\n</div>';
	
	function toArray(arr, start, end) {
	    return Array.prototype.slice.call(arr, start || 0, end || arr.length);
	}
	var tempFn = xtpl(TEMP_STR); //  模板函数
	var platform = $.os.ios ? 'ios' : 'android';
	
	/**
	 * 伪观察者，同名事件会相互覆盖
	 */
	
	var Observer = function () {
	    function Observer() {
	        _classCallCheck(this, Observer);
	
	        this._events = {};
	    }
	
	    _createClass(Observer, [{
	        key: 'on',
	        value: function on(type, fn) {
	            this._events[type] = fn;
	            return this;
	        }
	    }, {
	        key: 'trigger',
	        value: function trigger(type) {
	            var fn = this._events[type];
	            if (fn) {
	                var args = toArray(arguments, 1);
	                fn.apply(this, args);
	            }
	            return this;
	        }
	    }, {
	        key: 'off',
	        value: function off(type) {
	            delete this._events[type];
	            this._events[type] = null;
	            return this;
	        }
	    }]);
	
	    return Observer;
	}();
	/**
	 * 键盘对象类
	 */
	
	
	var Keyboard = function (_Observer) {
	    _inherits(Keyboard, _Observer);
	
	    function Keyboard() {
	        _classCallCheck(this, Keyboard);
	
	        var _this = _possibleConstructorReturn(this, (Keyboard.__proto__ || Object.getPrototypeOf(Keyboard)).call(this));
	
	        _this.height = 0;
	
	        var str = '<div id="ui-keyboard" class="ukey-hide" ' + platform + '><div class="ukey-content ukey-transfrom-hide"></div></div>';
	        _this.$wrap = $(str);
	        _this.$content = _this.$wrap.find('.ukey-content');
	        $('body').append(_this.$wrap);
	
	        _this.bindEvent();
	        return _this;
	    }
	
	    _createClass(Keyboard, [{
	        key: 'show',
	        value: function show() {
	            var _this2 = this;
	
	            this.$wrap.removeClass('ukey-hide');
	            this.setHeight();
	            setTimeout(function () {
	                _this2.$content.removeClass('ukey-transfrom-hide');
	            }, 20);
	            return this;
	        }
	    }, {
	        key: 'hide',
	        value: function hide() {
	            var _this3 = this;
	
	            this.trigger('hide');
	            this.$content.addClass('ukey-transfrom-hide');
	            setTimeout(function () {
	                _this3.$wrap.addClass('ukey-hide');
	            }, 300);
	            return this;
	        }
	    }, {
	        key: 'bindEvent',
	        value: function bindEvent() {
	            var me = this;
	            this.$content.on('touchend', '.ukey-cel', function () {
	                var that = $(this);
	                if (that.hasClass('ukey-disable')) {
	                    return;
	                }
	                var value = that.data('value');
	                me.trigger('tap', value);
	                return false;
	            }).on('touchend', '.ukey-tools-btn', function () {
	                me.trigger('tap', 'ok');
	                return false;
	            });
	        }
	    }, {
	        key: 'setHeight',
	        value: function setHeight() {
	            this.$wrap.css('height', this.height + 'rem');
	            this.$content.css('height', this.height + 'rem');
	            return this;
	        }
	    }, {
	        key: 'changeType',
	        value: function changeType(type) {
	            var data = void 0;
	            switch (type) {
	                case 'id':
	                case 'number':
	                case 'tel':
	                    data = META_DATA[type];
	                    this.height = META_DATA.height.n9[platform];
	                    break;
	                case 'plate':
	                    data = META_DATA.plate;
	                    this.height = META_DATA.height.plate[platform];
	                    break;
	                default:
	                    console.error('不支持的键盘类型');
	            }
	            this.$content.html(tempFn(data));
	            return this;
	        }
	    }]);
	
	    return Keyboard;
	}(Observer);
	
	/**
	 * 光标类， 自定义zepto事件：
	 * kb:input
	 * kb:end
	 * kb:start
	 */
	
	
	var Cursor = function () {
	    function Cursor() {
	        _classCallCheck(this, Cursor);
	    }
	
	    _createClass(Cursor, [{
	        key: 'start',
	        value: function start($input) {
	            var _this4 = this;
	
	            this.clean();
	            this.$input = $input;
	            this.inputType = this.$input.data('type');
	            this.maxlength = this.$input.data('maxlength') || 0;
	            this.max = this.$input.data('max') || 0;
	
	            this.$input.addClass('ukey-focus');
	            keyboard.on('tap', function (v) {
	                return _this4.tapEvent(v);
	            }).on('hide', function () {
	                return _this4.destroy();
	            }).changeType(this.inputType).show();
	            this.autoHeight(); // 自动设置滚动高度
	            this.$input.triggerHandler('kb:start');
	            return this;
	        }
	    }, {
	        key: 'checkVal',
	        value: function checkVal(oldVal, v) {
	            var ret = void 0;
	            if (!this.maxlength || oldVal.length < this.maxlength) {
	                // 最大长度校验
	                ret = oldVal + v;
	            } else {
	                return null;
	            }
	            if (this.inputType === 'tel' || this.inputType === 'number') {
	                // 数字类型校验
	                var nRet = Number(ret);
	                if (isNaN(nRet)) {
	                    return null;
	                }
	                if (this.max && nRet > this.max) {
	                    // 大于最大值
	                    return null;
	                }
	            }
	            if (this.$input.triggerHandler('kb:input', [v, ret, oldVal]) === false) {
	                // 外部校验
	                return null;
	            }
	            return ret;
	        }
	    }, {
	        key: 'tapEvent',
	        value: function tapEvent(v) {
	            v = String(v);
	            var oldVal = String(this.$input.data('value') || '');
	            var newVal = null;
	            switch (v) {
	                case 'del':
	                    if (this.$input.triggerHandler('kb:del') !== false) {
	                        if (oldVal.length > 0) {
	                            newVal = oldVal.substr(0, oldVal.length - 1);
	                        }
	                    }
	                    break;
	                case 'ok':
	                    if (this.$input.triggerHandler('kb:ok') !== false) {
	                        keyboard.hide();
	                    }
	                    break;
	                default:
	                    newVal = this.checkVal(oldVal, v);
	            }
	            if (newVal !== null) {
	                this.$input.data('value', newVal);
	                this.setShowName();
	            }
	            return false;
	        }
	    }, {
	        key: 'setShowName',
	        value: function setShowName($el) {
	            // 处理 placeholder 和光标位置
	            if (!$el) {
	                $el = this.$input;
	            }
	            var val = String($el.data('value') || '');
	            if (val === '') {
	                $el.removeClass('ukey-cursor-after').addClass('ukey-cursor-before');
	                $el.html($el.data('placeholder') || '');
	            } else {
	                $el.addClass('ukey-cursor-after').removeClass('ukey-cursor-before');
	                $el.html(val);
	            }
	        }
	    }, {
	        key: 'clean',
	        value: function clean() {
	            if (this.$input) {
	                // del另外一个focus
	                this.$input.removeClass('ukey-focus');
	                this.$input = null;
	                this.inputType = null;
	            }
	            return this;
	        }
	    }, {
	        key: 'autoHeight',
	        value: function autoHeight() {
	            // 自动设置页面滚动高度，使输入框可见
	            var $win = $(window);
	            var kbHeight = keyboard.$wrap.height();
	            var offset = this.$input.offset();
	            var inputHeight = offset.top + this.$input.height();
	            var bodyScroll = $win.scrollTop();
	            if (inputHeight - bodyScroll > kbHeight) {
	                $win.scrollTop(bodyScroll + kbHeight);
	            }
	        }
	    }, {
	        key: 'destroy',
	        value: function destroy() {
	            this.$input.triggerHandler('kb:end');
	            this.clean();
	            keyboard.on('hide', function () {});
	            keyboard.hide();
	            return this;
	        }
	    }]);
	
	    return Cursor;
	}();
	
	var keyboard = void 0;
	var cursor = void 0;
	function init() {
	    if (!keyboard) {
	        keyboard = new Keyboard();
	        cursor = new Cursor();
	    }
	}
	function start($input) {
	    init();
	    cursor.start($input);
	}
	function scan() {
	    init();
	    var $kbs = $('[keyboard]').removeAttr('keyboard');
	    if ($kbs.length) {
	        $kbs.on('tap', function () {
	            start($(this));
	        });
	        $kbs.each(function () {
	            cursor.setShowName($(this));
	        });
	    }
	}
	
	$(document).ready(scan);
	
	exports.start = start;
	exports.close = function () {
	    return keyboard.hide();
	};
	exports.scan = scan;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(3);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(5)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./node_modules/css-loader/index.js!./index.css", function() {
				var newContent = require("!!./node_modules/css-loader/index.js!./index.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(4)();
	// imports
	
	
	// module
	exports.push([module.id, "#ui-keyboard,\n#ui-keyboard *{\n    box-sizing: border-box;\n    padding: 0;\n    margin: 0;\n}\n#ui-keyboard{\n    -moz-user-select: none;\n    -o-user-select:none;\n    -khtml-user-select:none;\n    -webkit-user-select:none;\n    -ms-user-select:none;\n    user-select:none;\n}\n#ui-keyboard .ukey-content{\n    transition: transform 0.3s;\n    transform: translate(0,0);\n    overflow: hidden;\n    \n    position: fixed;\n    left:0;\n    right:0;\n    bottom:0;\n    width: 7.5rem;\n    z-index: 999;\n}\n#ui-keyboard .ukey-transfrom-hide{\n    transform: translate(0,100%);\n}\n.ukey-hide{ display: none !important; }\n/**\n * ios\n */\n#ui-keyboard[ios] .ukey-icon-delete{\n    width: 0.73rem;\n    height: 0.54rem;\n    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAiCAMAAAAXpRt+AAAAWlBMVEUAAAADAwMDAwMmJiYEBAQEBAQDAwMFBQUEBARVVVUFBQUEBAQHBwcLCwsEBAQEBAQDAwMDAwMEBAQDAwMEBAQEBAQFBQUEBAQGBgYbGxsEBAQEBAQFBQUDAwPF3K3QAAAAHXRSTlMA+UkGtoDpLoUDXNEkFTo49dutlHt4YEYnCcXEYmhJRkAAAADOSURBVDjLldXJFoMgDAVQcAC1orVVa4f8/2+Wjbwjg5i3YnHFKQnCi1m+iiJRVdeKIMudkrn1Hi4fdBbVH3VFRNOqI3ctdSPt/q2vn2+RyGB95+lapNMQVVmNaCJ1QTtiRV4jjnva1AOQXkvw6N41yWJfF5I68JgWo7TeaanBfQ1UYAEOHXpocOjQOw3eQAfeaXAFHXhoBmc+DPdVuR+S+Zt4RbDxSuyVLeAR1267Fvz2YDYft7X5g4M/luCnX2bosUcqMl8a2IiZP2fHwR/dsSvM9kfu2gAAAABJRU5ErkJggg==');\n    background-size: cover;\n    background-repeat: no-repeat;\n    display: inline-block;\n    vertical-align: middle;\n}\n@media screen and (max-width: 320px) {\n    #ui-keyboard[ios] .ukey-icon-delete{\n        width: 0.74rem;\n    }\n}\n\n#ui-keyboard[ios]{\n    width: 100%;\n    height: 5.2rem;\n    font-size: 0.5rem;\n}\n#ui-keyboard[ios] .ukey-tools{\n    height: 0.84rem;\n    line-height: 0.84rem;\n    background: #EAEBEF ;\n    text-align: right;\n}\n#ui-keyboard[ios] .ukey-tools .ukey-tools-btn{\n    height: 0.84rem;\n    line-height: 0.84rem;\n    vertical-align: top;\n    color:#4881F7;\n    font-size: 0.32rem;\n    display: inline-block;\n    padding: 0 0.12rem;\n}\n#ui-keyboard[ios] .ukey-content{\n    background-color: #D3D7DE;\n}\n#ui-keyboard[ios] .ukey-row{\n    text-align: center;\n}\n#ui-keyboard[ios] .ukey-row .ukey-cel{ /*通用单元格设置*/\n    position: relative;\n\n    display: inline-block;\n    border-radius: 5px;\n    box-shadow: 0 2px 0 #888A8E;\n    background-color: #FFF;\n}\n#ui-keyboard[ios] .ukey-p .ukey-cel:before,\n#ui-keyboard[ios] .ukey-n .ukey-cel:before,\n#ui-keyboard[ios] .ukey-s .ukey-cel:before{ /*扩大可点击区域*/\n    content: \" \";\n    position: absolute;\n    top: -0.11rem;\n    bottom: 0;\n    left: -0.059rem;\n    right: -0.059rem;\n}\n#ui-keyboard[ios] .ukey-p .ukey-cel{ /*车牌特定前缀单元格*/\n    margin: 0.11rem 0.059rem 0;\n    width: 1.38rem;\n    height: 0.82rem;\n    line-height: 0.82rem;\n    color: #000;\n    background-color: #ADB3BC;\n    font-size: 0.4rem;\n}\n\n#ui-keyboard[ios] .ukey-n .ukey-cel{ /*数字单元格*/\n    margin: 0.12rem 0.059rem 0;\n    width: 0.63rem;\n    height: 0.86rem;\n    line-height: 0.86rem;\n    color: #000;\n    background-color: #ADB3BC;\n}\n#ui-keyboard[ios] .ukey-s .ukey-cel{ /*字符串单元格*/\n    margin: 0.20rem 0.059rem 0;\n    width: 0.63rem;\n    height: 0.861rem;\n    line-height: 0.86rem;\n}\n#ui-keyboard[ios] .ukey-9 .ukey-gray{ background: none;}\n#ui-keyboard[ios] .ukey-9 .ukey-cel{ /*9宫格*/\n    display: inline-block;\n    box-shadow: none;\n    height: 1.081rem;\n    line-height: 1.081rem;\n    width: 2.5rem;\n    border-radius: 0;\n\n    border-right: 0.5px solid #D3D7DE;\n    border-top: 0.5px solid #D3D7DE;\n}\n#ui-keyboard[ios] .ukey-9 .ukey-cel:nth-child(3n) {\n    border-right: none\n}\n#ui-keyboard[ios] .ukey-9 .ukey-cel:nth-child(1),\n#ui-keyboard[ios] .ukey-9 .ukey-cel:nth-child(2),\n#ui-keyboard[ios] .ukey-9 .ukey-cel:nth-child(3) {\n    border-top: none\n}\n\n#ui-keyboard[ios] .ukey-delete{ background-color: #ADB3BC; }\n#ui-keyboard[ios] .ukey-delete .ukey-icon-delete{\n    width: 0.485rem;\n    height: 0.36rem;\n}\n#ui-keyboard[ios] .ukey-s .ukey-ok{\n    width: 1.75rem;\n    height: 0.84rem;\n    line-height: 0.84rem;\n    color: #FFF;\n    background-color: #007AFF;\n    box-shadow: 0 2px 0 #6A6471;\n}\n\n/**\n * android\n */\n#ui-keyboard[android] .ukey-icon-delete{\n    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAAA2BAMAAACW68LFAAAALVBMVEU3SE4AAAA4SVA5SU44SlA4SFA3SVA4SVA7Tk44SE83SE83SFE3R1BEVVU6SVD/EYxRAAAAD3RSTlOZAJYxM41maQ1xeDxZDyO2/lp3AAABDUlEQVQ4y4yQvQkCQRQGP0QQNXooXHpgZuZWYCZmlmAJCrZgAZZiKXYkws6TJ+7PRAf33cztyuB40l+2T/PRoBLzl4/OKvJgtFaZJaO7yizUrmkialWoVaFWpaemnpp6avqp3UY/+EFOrM0s8bixiyDWrrYas2hngyDXEJklRPZVhdr088mYRWZ7H1HzVwkRZSmcjXesgVpQMQZqQcUWqAUVIqAWVIiAWlAhAmpBhQioBRUioBZUiJqj7lz7xzuvoH2Z7zkxkBothCOYcFIhnOhISb6YGUEYPSMQzlKEMyfp2ZxwgYGwj/JCDGEf4YKV8iKasH3EVhuEKyCSqjLBA0RUioIyxFSvgs9wV9QAlVVDMJwG7YEAAAAASUVORK5CYII='); \n    background-size: cover;\n    background-repeat: no-repeat;\n    display: inline-block;\n    vertical-align: middle;\n    width: 0.48rem;\n    height: 0.36rem;\n}\n\n#ui-keyboard[android]{\n    width: 100%;\n    height: 4.4rem;\n    font-size: 0.36rem;\n}\n#ui-keyboard[android] .ukey-content{\n    background: #FAFAFA;\n}\n#ui-keyboard[android] .ukey-tools{\n    height: 0.96rem;\n    background: #F5F5F5;\n    box-shadow: 0 4px 4px 0 rgba(0,0,0,0.24), 0 0 4px 0 rgba(0,0,0,0.12), inset 0 1px 0 0 rgba(255,255,255,0.80);\n    text-align: right;\n}\n#ui-keyboard[android] .ukey-tools .ukey-tools-btn{\n    height: 0.96rem;\n    line-height: 0.96rem;\n    vertical-align: top;\n    color:#4881F7;\n    font-size: 0.32rem;\n    display: inline-block;\n    padding: 0 0.12rem;\n}\n#ui-keyboard[android] .ukey-keys{\n    height: 100%;\n    background: #FCFCFC;\n    height: 100%;\n    box-shadow: 0 0 4px 0 rgba(0,0,0,0.12), inset 0 1px 0 0 rgba(255,255,255,0.80);\n}\n#ui-keyboard[android] .ukey-row{\n    text-align: center;\n}\n#ui-keyboard[android] .ukey-row .ukey-cel{  /*通用单元格设置*/\n    position: relative;\n\n    display: inline-block;\n    border-radius: 2px;\n    background: #FAFAFA;\n    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.08), 0 0 2px 0 rgba(0,0,0,0.12), inset 0 1px 0 0 rgba(255,255,255,0.80);\n}\n#ui-keyboard[android] .ukey-s .ukey-cel:before,\n#ui-keyboard[android] .ukey-n .ukey-cel:before,\n#ui-keyboard[android] .ukey-p .ukey-cel:before{ /*扩大可点击区域*/\n    content: \" \";\n    position: absolute;\n    top: -0.12rem;\n    bottom: 0;\n    left: -0.06rem;\n    right: -0.06rem;\n}\n#ui-keyboard[android] .ukey-p .ukey-cel{ /*车牌特定前缀单元格*/\n    margin: 0.24rem 0.06rem 0;\n    width: 1.36rem;\n    height: 0.7rem;\n    line-height: 0.7rem;\n    color: #000;\n    font-size: 0.4rem;\n}\n\n#ui-keyboard[android] .ukey-n .ukey-cel{  /*数字单元格*/\n    margin: 0.12rem 0.06rem 0;\n    width: 0.618rem;\n    height: 0.7rem;\n    line-height: 0.7rem;\n    color: #000;\n}\n#ui-keyboard[android] .ukey-s .ukey-cel{ /*字符串单元格*/\n    margin: 0.12rem 0.06rem 0;\n    width: 0.618rem;\n    height: 0.70rem;\n    line-height: 0.70rem;\n}\n#ui-keyboard[android] .ukey-9 .ukey-cel{ /*9宫各*/\n    width: 2.3rem;\n    height: 0.96rem;\n    line-height: 0.96rem;\n    margin: 0.075rem 0.085rem;\n}\n#ui-keyboard[android] .ukey-s .ukey-ok{\n    width: 1.66rem;\n    height: 0.7rem;\n    line-height: 0.7rem;\n    color: #4881F7;\n}\n\n/**\n * 更高优先级的common\n */\n#ui-keyboard .ukey-disable{ color: #DDD !important; }\n#ui-keyboard .ukey-row .ukey-cel:active{\n    background-color: #D3D7DE;\n}\n/**\n * 虚拟光标\n */\n@keyframes ukey-flicker{\n    0%{\n        opacity: 1;\n    }\n    49%{\n        opacity: 1;\n    }\n    50%{\n        opacity: 0\n    }\n    100%{\n        opacity: 0;\n    }\n}\n.ukey-focus{\n    overflow: hidden;\n    white-space:nowrap;\n}\n.ukey-focus.ukey-cursor-before:before,\n.ukey-focus.ukey-cursor-after:after\n{\n    content: \" \";\n    \n    width: 2px;\n    height: 80%;\n    display: inline-block;\n    vertical-align: middle;\n    background-color: #4881F7;\n\n    animation-duration: 1.2s;\n    animation-name: ukey-flicker;\n    animation-timing-function: linear;\n    animation-iteration-count: infinite;\n}\n.ukey-cursor-before{\n    color: #aaa !important;\n}\n", ""]);
	
	// exports


/***/ },
/* 4 */
/***/ function(module, exports) {

	"use strict";
	
	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function () {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for (var i = 0; i < this.length; i++) {
				var item = this[i];
				if (item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function (modules, mediaQuery) {
			if (typeof modules === "string") modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for (var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if (typeof id === "number") alreadyImportedModules[id] = true;
			}
			for (i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if (typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if (mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if (mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	*
	* @param tpl {String} 模板字符串
	* @param data {Object} 模板数据（不传或为null时返回渲染方法）
	*
	* @return {String} 渲染结果
	* @return {Function} 渲染方法
	*
	*/
	
	function parse(p0) {
	    // 指令解析
	    var dir = p0.trim().replace(/[\s]+/g, ' ').split(' ');
	    switch (dir[0]) {
	        case 'if':
	            return ';if(' + dir.slice(1).join(' ') + '){';
	        case 'else':
	            return '}else{';
	        case 'elseif':
	            return '}else if(' + dir.slice(1).join(' ') + '){';
	        case 'endif':
	            return '};';
	        case 'each':
	            return '; ' + dir[1] + '.forEach(function(' + dir[3] + ', ' + dir[4] + '){';
	        case 'endeach':
	            return '});';
	        case 'for':
	            return ';(function(){ for(var ' + dir[4] + ' in ' + dir[1] + '){ var ' + dir[3] + '=' + dir[1] + '[' + dir[4] + '];';
	        case 'endfor':
	            return '}}());';
	        default:
	            // 默认直接输出变量, 过滤 undefined
	            //return `;$+=(typeof ${dir[0]}=='undefined'?'':${dir[0]}) ;` ;
	            return ';$+=(typeof (' + p0 + ')===\'undefined\'?\'\':(' + p0 + '));';
	    }
	}
	module.exports = function (tpl, data) {
	    var fn = function fn(d) {
	        var i = void 0,
	            k = [],
	            v = [];
	        for (i in d) {
	            k.push(i);
	            v.push(d[i]);
	        }
	        return new Function(k, fn.$).apply(d, v);
	    };
	    if (!fn.$) {
	        fn.$ = 'var $="";';
	        var tpls = tpl.split('{{');
	        for (var t = 0, len = tpls.length; t < len; t++) {
	            var p = tpls[t].split('}}');
	            if (t !== 0) {
	                fn.$ += parse(p[0]);
	            }
	            fn.$ += '$+="';
	            fn.$ += p[p.length - 1].replace(/\'/g, "\\'").replace(/\"/g, '\\"').replace(/\r\n/g, '\\n').replace(/\n/g, '\\n').replace(/\r/g, '\\n');
	            fn.$ += '"';
	        }
	        fn.$ += ';return $;';
	        //console.log(fn.$);
	    }
	    return data ? fn(data) : fn;
	};

/***/ }
/******/ ]);
//# sourceMappingURL=test.js.map