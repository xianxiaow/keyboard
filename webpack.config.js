
module.exports = {
    devtool : 'source-map',
    entry : {
        test: './test/test-es6.js'
    },
    output : {
        path : './test',
        filename: 'test.js'
    },
    module: {
        loaders: [
            { test: /\.css$/,    loader: "style-loader!css-loader" },
            { test: /\.jsx?$/,   loader: "babel-loader", query: { presets: ['es2015'] } }
        ]
    },
    resolve:{
        modulesDirectories:['node_modules']
    }
}

