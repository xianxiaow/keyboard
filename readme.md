# keyboard

HTML移动端虚拟键盘, 基于zepto。
引用模块，在dom ready事件之后，会自动扫描页面绑定keyboard属性的元素。

## Use Guide

```
// HTML
<div keyboard
id="k1"
data-type="plate"
data-placeholder="请输入车牌号"
></div>

//JS
require('@ui/keyboard');
```

## API接口
```
const keyboard = require('@ui/keyboard');

/**
 * 扫描页面keyboard元素
 * 在dom ready之后会自动调用一次这个接口，
 * 如果是动态生成的元素，则需要手动调用
 */
keyboard.scan();

/**
 * 关闭虚拟键盘
 */
keyboard.close();

/**
 * 开启虚拟键盘
 * @params $input{Zepto}
 */
keyboard.start($input);

// 虚拟键盘的值统一保存在data-value属性中
console.log($input.data('value'));
```
## 自定义事件

```
const $k1 = $('#k1');

/**
 * kb:start 开始输入事件
 */
$k1.on('kb:start', function(event){ });

/**
 * kb:end 输入完成事件 
 */
$k1.on('kb:end', function(event){ });

/**
 * kb:input 值改变事件
 * @param evevt{Object}  事件对象
 * @param c{String} 本地输入的字符
 * @param newVal{String} 如果输入生效，新的值
 * @param oldVal{String} 旧的值
 * @return {Boolean} 如果返回false, 则本次输入无效
 */
$k1.on('kb:input', function(event, c, newVal, oldVal){
    if(newVal === '123456'){
        return false;
    }
})
/**
 * kb:del 删除事件
 * @param event{Object} 事件对象
 * @return {Boolean} 如果返回false, 则删除失败
 */
$k1.on('kb:del', function(event) {
    if(true){
        return false;
    }
});
/**
 * kb:ok 完成按钮点击事件
 * @param event{Object} 事件对象
 * @return {Boolean} 如果返回false, 则不关闭软键盘
 */
$k1.on('kb:ok', function(event) {
    if(true){
        return false;
    }
})
```

## 配置项

| 属性名 | 必选 | 说明 |
|----    | ---- | ---- |
| keyboard | 是 | 说明该元素是虚拟键盘元素 |
| data-type | 是 | 输入类型，目前支持id(身份证)/number/tel/plate(车牌) |
| data-maxlength | 否 | 最大长度 |
| data-max | 否 | 最大值，仅对number/tel类型生效 |
| data-placeholder | 否 | 输入提示 |

## 内部实现原理

核心对象： 
 + Keyboard 键盘对象。控制键盘显隐，切换键盘键值
 + Cursor 光标对象。接受Keyboard的键值，将外部的元素（如：div）转换为内部input元素。

 内部采用单例模式，只保存一个Keyboard和Cursor实例



