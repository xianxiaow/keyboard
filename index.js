require('./index.css');
const xtpl = require('@util/xtpl');

/**
 * JS 虚拟键盘
 */

const META_DATA = {
    height: {
        plate: {
            ios : 5.2,
            android: 4.38
        },
        n9: {
            ios : 5.159,
            android: 5.44
        }
    },
    id: {  // 身份证输入
        need_tools: true,
        rows: [{
            style: 'ukey-9',
            cel: [{
                value: 1
            },{
                value: 2
            },{
                value: 3
            },{
                value: 4
            },{
                value: 5
            },{
                value: 6
            },{
                value: 7
            },{
                value: 8
            },{
                value: 9
            },{
                style: 'ukey-gray',
                value: 'X'
            },{
                value: 0
            },{
                style: 'ukey-gray',
                value: 'del',
                name: '<i class="ukey-icon-delete"></i>'
            }]
        }]
    },
    number: {
        need_tools: true,
        rows: [{
            style: 'ukey-9',
            cel: [{
                value: 1
            },{
                value: 2
            },{
                value: 3
            },{
                value: 4
            },{
                value: 5
            },{
                value: 6
            },{
                value: 7
            },{
                value: 8
            },{
                value: 9
            },{
                style: 'ukey-gray',
                value: '.'
            },{
                value: 0
            },{
                style: 'ukey-gray',
                value: 'del',
                name: '<i class="ukey-icon-delete"></i>'
            }]
        }]
    },
    tel: {
        need_tools: true,
        rows: [{
            style: 'ukey-9',
            cel: [{
                value: 1
            },{
                value: 2
            },{
                value: 3
            },{
                value: 4
            },{
                value: 5
            },{
                value: 6
            },{
                value: 7
            },{
                value: 8
            },{
                value: 9
            },{
                style: 'ukey-gray',
                value: '',
                name: '&nbsp;'
            },{
                value: 0
            },{
                style: 'ukey-gray',
                value: 'del',
                name: '<i class="ukey-icon-delete"></i>'
            }]
        }] 
    },
    plate: {
        need_tools: false,
        rows: [{
            style: 'ukey-p',
            cel: [{
                value: '港'
            },{
                value: '澳'
            },{
                style: 'ukey-disable',
                value: '警'
            },{
                style: 'ukey-disable',
                value: '学'
            },{
                style: 'ukey-disable',
                value: '领'
            }]
        },{
            style: 'ukey-n',
            cel: [{
                value: 1
            },{
                value: 2
            },{
                value: 3
            },{
                value: 4
            },{
                value: 5
            },{
                value: 6
            },{
                value: 7
            },{
                value: 8
            },{
                value: 9
            },{
                value: 0
            },]
        },{
            style: 'ukey-s',
            cel: [{
                value: 'Q'
            },{
                value: 'W'
            },{
                value: 'E'
            },{
                value: 'R'
            },{
                value: 'T'
            },{
                value: 'Y'
            },{
                value: 'U'
            },{
                style: 'ukey-disable',
                value: 'I'
            },{
                style: 'ukey-disable',
                value: 'O'
            },{
                value: 'P'
            }]
        },{
            style: 'ukey-s',
            cel: [{
                value: 'A'
            },{
                value: 'S'
            },{
                value: 'D'
            },{
                value: 'F'
            },{
                value: 'G'
            },{
                value: 'H'
            },{
                value: 'J'
            },{
                value: 'K'
            },{
                value: 'L'
            },{
                style: 'ukey-delete',
                value: 'del',
                name: '<i class="ukey-icon-delete"></i>'
            }]
        },{
            style: 'ukey-s',
            cel: [{
                value: 'Z'
            },{
                value: 'X'
            },{
                value: 'C'
            },{
                value: 'V'
            },{
                value: 'B'
            },{
                value: 'N'
            },{
                value: 'M'
            },{
                style: 'ukey-ok',
                value: 'ok',
                name: '完成'
            }]
        }]
    }
}

/**
 * 字符串模板
 */
const TEMP_STR = `
{{ if need_tools }}
<div class="ukey-tools">
    <div class="ukey-tools-btn">完成</div>
</div>
{{ endif }}
<div class="ukey-keys">
{{ each rows as row }}
    <div class="ukey-row {{ row.style }}">
    {{ each row.cel as cel }}<div class="ukey-cel {{ cel.style }}" data-value="{{ cel.value }}">{{ cel.name || cel.value }}</div>{{ endeach }}
    </div>
{{ endeach }}
</div>`;

function toArray(arr, start, end) {
    return Array.prototype.slice.call(arr, start || 0, end || arr.length);
}
const tempFn = xtpl(TEMP_STR); //  模板函数
const platform = $.os.ios ? 'ios' : 'android';

/**
 * 伪观察者，同名事件会相互覆盖
 */
class Observer{
    constructor(){
        this._events = {}
    }
    on(type, fn){
        this._events[type] = fn;
        return this;
    }
    trigger(type){
        const fn = this._events[type];
        if (fn) {
            const args = toArray(arguments, 1);
            fn.apply(this, args);
        }
        return this;
    }
    off(type){
        delete this._events[type];
        this._events[type] = null;
        return this;
    }
}
/**
 * 键盘对象类
 */
class Keyboard extends Observer{
    constructor(){
        super();
        this.height = 0;

        const str = `<div id="ui-keyboard" class="ukey-hide" ${platform}><div class="ukey-content ukey-transfrom-hide"></div></div>`;
        this.$wrap = $(str);
        this.$content = this.$wrap.find('.ukey-content');
        $('body').append(this.$wrap);

        this.bindEvent();
    }
    show(){
        this.$wrap.removeClass('ukey-hide');
        this.setHeight();
        setTimeout(() => {
            this.$content.removeClass('ukey-transfrom-hide');
        }, 20);
        return this;
    }
    hide(){
        this.trigger('hide');
        this.$content.addClass('ukey-transfrom-hide');
        setTimeout(() => {
            this.$wrap.addClass('ukey-hide');
        },300);
        return this;
    }
    bindEvent() {
        const me = this;
        this.$content.on('touchend', '.ukey-cel', function(){
            const that = $(this);
            if (that.hasClass('ukey-disable')) {
                return;
            }
            const value = that.data('value');
            me.trigger('tap', value);
            return false;
        }).on('touchend', '.ukey-tools-btn', function(){
            me.trigger('tap', 'ok');
            return false;
        });
    }
    setHeight() {
        this.$wrap.css('height', `${this.height}rem`);
        this.$content.css('height', `${this.height}rem`);
        return this;
    }
    changeType(type) {
        let data;
        switch(type){
            case 'id':
            case 'number':
            case 'tel':
                data = META_DATA[type];
                this.height = META_DATA.height.n9[platform];
                break;
            case 'plate':
                data = META_DATA.plate;
                this.height = META_DATA.height.plate[platform];
                break;
            default:
                console.error('不支持的键盘类型');
        }
        this.$content.html(tempFn(data));
        return this;
    }
}

/**
 * 光标类， 自定义zepto事件：
 * kb:input
 * kb:end
 * kb:start
 */
class Cursor{
    constructor() {}
    start($input) {
        this.clean();
        this.$input = $input;
        this.inputType = this.$input.data('type');
        this.maxlength = this.$input.data('maxlength') || 0;
        this.max = this.$input.data('max') || 0;

        this.$input.addClass('ukey-focus');
        keyboard.on('tap', (v) => this.tapEvent(v))
                .on('hide', () => this.destroy())
                .changeType(this.inputType)
                .show();
        this.autoHeight(); // 自动设置滚动高度
        this.$input.triggerHandler('kb:start');
        return this;
    }
    checkVal(oldVal, v) {
        let ret ;
        if (!this.maxlength || oldVal.length < this.maxlength) { // 最大长度校验
            ret = oldVal + v;
        }else{
            return null;
        }
        if (this.inputType === 'tel' || this.inputType === 'number') {
            // 数字类型校验
            const nRet = Number(ret);
            if (isNaN(nRet)) {
                return null;
            }
            if (this.max && nRet>this.max) { // 大于最大值
                return null;
            }
        }
        if (this.$input.triggerHandler('kb:input', [v, ret, oldVal]) === false) { // 外部校验
            return null;
        }
        return ret;
    }
    tapEvent(v){
        v = String(v);
        const oldVal = String(this.$input.data('value') || '');
        let newVal = null;
        switch(v){
            case 'del':
                if (this.$input.triggerHandler('kb:del') !== false) {
                    if (oldVal.length > 0) {
                        newVal = oldVal.substr(0, oldVal.length - 1);
                    }
                }
                break;
            case 'ok':
                if (this.$input.triggerHandler('kb:ok') !== false) {
                    keyboard.hide();
                }
                break;
            default:
                newVal = this.checkVal(oldVal, v);
        }
        if (newVal !== null) {
            this.$input.data('value', newVal);
            this.setShowName();  
        }
        return false;
    }
    setShowName($el){ // 处理 placeholder 和光标位置
        if (!$el) {
            $el = this.$input;
        }
        const val = String($el.data('value') || '');
        if (val === '') {
            $el.removeClass('ukey-cursor-after').addClass('ukey-cursor-before');
            $el.html($el.data('placeholder') || '');
        }else{
            $el.addClass('ukey-cursor-after').removeClass('ukey-cursor-before');
            $el.html(val);
        }
    }
    clean(){
        if (this.$input) { // del另外一个focus
            this.$input.removeClass('ukey-focus');
            this.$input = null;
            this.inputType = null;
        }
        return this;
    }
    autoHeight() { // 自动设置页面滚动高度，使输入框可见
        const $win = $(window);
        const kbHeight = keyboard.$wrap.height();
        const offset = this.$input.offset();
        const inputHeight = offset.top + this.$input.height();
        const bodyScroll = $win.scrollTop();
        if (inputHeight - bodyScroll > kbHeight) {
            $win.scrollTop(bodyScroll + kbHeight);
        }
    }
    destroy(){
        this.$input.triggerHandler('kb:end');
        this.clean();
        keyboard.on('hide', () => {});
        keyboard.hide();
        return this;
    }
}


let keyboard;
let cursor;
function init() {
    if (!keyboard) {
        keyboard = new Keyboard();
        cursor = new Cursor();
    }
}
function start($input){
    init();
    cursor.start($input);
}
function scan(){
    init();
    const $kbs = $('[keyboard]').removeAttr('keyboard');
    if ($kbs.length) {
        $kbs.on('tap', function() {
            start($(this));
        });
        $kbs.each(function() {
            cursor.setShowName($(this));
        });
    }
}

$(document).ready(scan);

exports.start = start;
exports.close = () => keyboard.hide();
exports.scan = scan;

